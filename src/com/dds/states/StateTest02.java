// ---------------------------------------
// StateTest01.java
//
// Cr�e: 19 mars 2013 - 11:41:44
// Auteur: Administrateur
// ---------------------------------------

package com.dds.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class StateTest02 extends BasicGameState
{

	public StateTest02()
	{
	}

	public int getID()
	{
		return 1;
	}

	public void init(	GameContainer graphicsContainer,
						StateBasedGame state) throws SlickException
	{
	}

	public void render(	GameContainer graphicsContainer,
						StateBasedGame state,
						Graphics graphics) throws SlickException
	{
		graphics.drawString("State " +
							this.getClass().getSimpleName(),0,0);
	}

	public void update(	GameContainer graphicsContainer,
						StateBasedGame state,
						int delta) throws SlickException
	{
	}

}
