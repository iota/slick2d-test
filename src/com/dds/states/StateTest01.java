// ---------------------------------------
// StateTest01.java
//
// Cr�e: 19 mars 2013 - 11:41:44
// Auteur: Administrateur
// ---------------------------------------

package com.dds.states;

import org.apache.log4j.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.dds.business.Controler;
import com.dds.business.Player;
import com.dds.business.PlayerManager;

public class StateTest01 extends BasicGameState
{
	PlayerManager	playerManager;
	Controler		controler;
	Player			player;
	UnicodeFont		font;

	/**
	 * Cherche le controler qui semble le plus adapt�.
	 * <p>
	 * Cette fonction n'existe qu'� d�faut d'un autre moyen simple
	 * permettant de trouver parmi les controleurs, lesquels sont
	 * utilisables. <br/>
	 * Le pr�dicat actuel pour la s�l�ction du meilleurs controleur
	 * est le nombre d'axe disponible.
	 * </p>
	 */
	private static int findMostProbableControler(Input input)
	{
		int controlerID = -1;
		int maxAxisCount = 0;

		for( int i = 0;i < input.getControllerCount();++i )
		{
			if( maxAxisCount < input.getAxisCount(i) )
			{
				maxAxisCount = input.getAxisCount(i);
				controlerID = i;
			}
		}

		return controlerID;
	}

	public StateTest01() throws SlickException
	{
		this.font = new UnicodeFont("fonts/arial.ttf",15,false,false);
		this.font.addAsciiGlyphs();
		this.font.getEffects()
					.add(new ColorEffect(java.awt.Color.white));
		this.font.loadGlyphs();
	}

	public int getID()
	{
		return 0;
	}

	public void init(GameContainer gameContainer,StateBasedGame state)
		throws SlickException
	{
		Logger.getRootLogger().trace("Init state: " +
										getID());

		this.playerManager = new PlayerManager();

		controler = new Controler(	findMostProbableControler(gameContainer.getInput()),
									gameContainer.getInput());

		
		this.player = this.playerManager.addPlayer('H',this.controler);
		this.player.setPosition(gameContainer.getWidth() / 2.f,
								gameContainer.getHeight() / 2.f);
		this.player.init();
		
		gameContainer.getGraphics().setAntiAlias(true);
	}

	public void render(	GameContainer graphicsContainer,
						StateBasedGame state,
						Graphics graphics) throws SlickException
	{
		
		// Affiche les valeurs des axes du controleur.
		
//		final float lineOffset = font.getLineHeight();
//		final float yPos = 600;
//		graphics.setColor(Color.white);
//		font.drawString(0,yPos -
//							lineOffset *
//							4,"Left angle: " +
//								controler.getLeftAxisAngle());
//		font.drawString(0,yPos -
//							lineOffset *
//							3,"Left distance: " +
//								controler.getLeftDistance());
//		font.drawString(0,yPos -
//							lineOffset *
//							2,"Right angle: " +
//								controler.getRightAxisAngle());
//		font.drawString(0,yPos -
//							lineOffset *
//							1,"Right distance: " +
//								controler.getRightDistance());

		player.render(graphicsContainer,graphics);
	}

	public void update(	GameContainer graphicsContainer,
						StateBasedGame state,
						int delta) throws SlickException
	{
		player.update(graphicsContainer,delta * 0.001f);
	}

}
