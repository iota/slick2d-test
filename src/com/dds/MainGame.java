// ---------------------------------------
// MainGame.java
//
// Cr�e: 19 mars 2013 - 11:31:23
// Auteur: Administrateur
// ---------------------------------------

package com.dds;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.dds.states.StateTest01;
import com.dds.states.StateTest02;

public class MainGame extends StateBasedGame
{
	public MainGame()
	{
		super("Slick Test 01");
	}

	public void initStatesList(GameContainer aContainer)
		throws SlickException
	{
		addState(new StateTest01());
		addState(new StateTest02());
	}

}
