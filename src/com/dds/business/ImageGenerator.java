// ---------------------------------------
// ImageGenerator.java
//
// Cr�e: 20 mars 2013 - 01:00:18
// Auteur: iota
// ---------------------------------------

package com.dds.business;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class ImageGenerator
{
	public static Image createImage(char character,Font font) throws SlickException
	{
		Image image = Image.createOffscreenImage(32,32);
		Graphics g = image.getGraphics();

		String chars = new String();
		chars += character;

		final float w = font.getWidth(chars);
		final float h = font.getHeight(chars);

		final float px = ( 32.f - w ) / 2.f;
		final float py = ( 32.f - h ) / 2.f;

		g.setColor(Color.white);
		g.setFont(font);
		g.drawString(chars,px,py);
		
		Image finalImage = new Image(32,32);
		finalImage.setCenterOfRotation(	finalImage.getWidth() / 2.f,
		                               	finalImage.getHeight() / 2.f);
		
		g.copyArea(finalImage,0,0);
		return finalImage;
	}
}
