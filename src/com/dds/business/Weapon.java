// ---------------------------------------
// Weapon.java
//
// Cr�e: 20 mars 2013 - 00:45:57
// Auteur: iota
// ---------------------------------------

package com.dds.business;

import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Vector2f;

public class Weapon
{
	private class Bullet
	{
		public Vector2f	position;
		public Vector2f	velocity;
		public float	rotation;
	}

	private LinkedList<Bullet>	bullets;
	private LinkedList<Bullet>	reservedBullets;
	private Vector2f			currentVelocity;
	private Image				bulletImage;

	private static final float BulletSpeed = 400.f;
	/**
	 * @throws SlickException
	 * 
	 */
	public Weapon() throws SlickException
	{
		this.bullets = new LinkedList<Bullet>();
		this.reservedBullets = new LinkedList<Bullet>();
		this.currentVelocity = new Vector2f();

		this.bulletImage = new Image("images/bullet.png");
		
		for( int i = 0;i < 500;++i )
		{
			this.reservedBullets.add(new Bullet());
		}
	}
	
	public void init() throws SlickException
	{
//		this.bulletImage = ImageGenerator.createImage(	'*',
//														new UnicodeFont("fonts/arial.ttf",
//																		10,
//																		true,
//																		false));
	}

	public void setRotation(Vector2f rightAxisValues)
	{
		this.currentVelocity = rightAxisValues;
	}

	public void fire(Vector2f position,float rotation)
	{
		Bullet bullet = reservedBullets.pollFirst();
		bullet.position = position;
		bullet.velocity = this.currentVelocity;
		bullet.velocity.x *= BulletSpeed;
		bullet.velocity.y *= BulletSpeed;
		bullet.rotation = rotation;

		bullets.push(bullet);
	}

	public void update(GameContainer graphicsContainer,float delta)
	{
		float vx;
		float vy;

		Bullet bullet;
		for( int i = 0;i < this.bullets.size(); )
		{
			bullet = this.bullets.get(i);
			
			vx = bullet.velocity.x *
					delta;
			vy = bullet.velocity.y *
					delta;

			bullet.position.x += vx;
			bullet.position.y += vy;

			if( bullet.position.x < 0 ||
				bullet.position.x > graphicsContainer.getWidth() ||
				bullet.position.y < 0 ||
				bullet.position.y > graphicsContainer.getHeight() )
			{
				this.bullets.remove(bullet);
				this.reservedBullets.push(bullet);
			}
			else
			{
				++i;
			}
		}
	}

	public void render(	GameContainer graphicsContainer,
						Graphics graphics)
	{
		for( Bullet bullet : this.bullets )
		{
			this.bulletImage.setRotation(bullet.rotation);
			graphics.drawImage(	this.bulletImage,
								bullet.position.x,
								bullet.position.y);
		}
	}
}
