package com.dds.business;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;


// ---------------------------------------
// Player.java
//
// Cr�e: 19 mars 2013 - 15:42:39
// Auteur: Administrateur
// ---------------------------------------

public class Player
{
	private final Controler	controler;
	private Vector2f		position;
	private float			bodyRotation;
	private final float		velocity	= 250.f;
	private Vector2f		speed;
	private final Image		shipImage;
	private Weapon			weapon;
	
	public Player(Controler controler,Image shipImage) throws SlickException
	{
		this.controler = controler;
		this.shipImage = shipImage;
		this.position = new Vector2f();
		this.speed = new Vector2f();
		this.weapon = new Weapon();
	}

	public void render(	GameContainer graphicsContainer,
						Graphics graphics)
	{
		graphics.setColor(Color.white);
		graphics.drawImage(shipImage,position.x,position.y);
		this.weapon.render(graphicsContainer,graphics);
	}

	public void setBodyRotation(float bodyRotation)
	{
		this.bodyRotation = bodyRotation;
	}

	public void setPosition(float x,float y)
	{
		this.position.set(x,y);
	}

	public void update(GameContainer graphicsContainer,float delta)
	{
		controler.update();
		speed = controler.getLeftAxisValues();
		bodyRotation = controler.getLeftAxisAngle();
		
		position.x += speed.x *
						delta *
						velocity;

		position.y += speed.y *
						delta *
						velocity;

		shipImage.setRotation(bodyRotation);
		
		if( controler.getRightDistance() > 0.3f )
		{
			weapon.setRotation(controler.getRightAxisValues());
			weapon.fire(getCenterPosition(),controler.getRightAxisAngle());
		}
		
		weapon.update(graphicsContainer,delta);
	}

	/**
	 *	@return
	 */
	private Vector2f getCenterPosition()
	{
		Vector2f pos = this.position.copy();
		pos.x += this.shipImage.getCenterOfRotationX();
		pos.y += this.shipImage.getCenterOfRotationY();
		return pos;
	}

	public void init() throws SlickException
	{
		this.weapon.init();
	}
}
