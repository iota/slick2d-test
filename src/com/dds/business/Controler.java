// ---------------------------------------
// Controler.java
//
// Cr�e: 19 mars 2013 - 14:14:06
// Auteur: Administrateur
// ---------------------------------------

package com.dds.business;

import org.apache.log4j.Logger;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;

public class Controler
{
	final int			controlerIdentifier;
	final Input			input;
	final static int	AxisXLeft	= 1;
	final static int	AxisYLeft	= 0;
	final static int	AxisXRight	= 3;
	final static int	AxisYRight	= 2;

	Vector2f			leftAnalogValues;
	float				leftAnalogAngle;
	float				leftAnalogDistance;
	Vector2f			rightAnalogValues;
	float				rightAnalogAngle;
	float				rightAnalogDistance;
	float				threshold	= 0.2f;

	private static float getAxisAngle(	int controlerID,
										int axisXID,
										int axisYID,
										Input input)
	{
		final float xAxis = input.getAxisValue(controlerID,axisXID);
		final float yAxis = input.getAxisValue(controlerID,axisYID);
		final double t = Math.atan2(yAxis *
									Math.PI,xAxis *
											Math.PI);
		return (float)Math.toDegrees(t) + 90.f;
	}

	private static float getAxisDistance(	int controlerID,
											int axisXID,
											int axisYID,
											Input input)
	{
		final float xAxis = input.getAxisValue(controlerID,axisXID);
		final float yAxis = input.getAxisValue(controlerID,axisYID);
		return (float)Math.sqrt(( xAxis * xAxis ) +
								( yAxis * yAxis ));
	}

	private static Vector2f getAxisValues(	int controlerID,
											int axisXID,
											int axisYID,
											Input input)
	{
		final float xAxis = input.getAxisValue(controlerID,axisXID);
		final float yAxis = input.getAxisValue(controlerID,axisYID);
		return new Vector2f(xAxis,yAxis);
	}

	public Controler(int controlerIdentifier,Input input)
	{
		this.controlerIdentifier = controlerIdentifier;
		this.input = input;
		leftAnalogValues = new Vector2f();
		rightAnalogValues = new Vector2f();

		final Logger log = Logger.getRootLogger();
		log.trace("Init controler: " +
					controlerIdentifier);
		
		reset();
	}

	public float getLeftAxisAngle()
	{
		return leftAnalogAngle;
	}

	public Vector2f getLeftAxisValues()
	{
		return leftAnalogValues;
	}

	public float getLeftDistance()
	{
		return leftAnalogDistance;
	}

	public float getRightAxisAngle()
	{
		return rightAnalogAngle;
	}

	public Vector2f getRightAxisValues()
	{
		return rightAnalogValues;
	}

	public float getRightDistance()
	{
		return rightAnalogDistance;
	}

	public void reset()
	{
		this.leftAnalogAngle = 0.f;
		this.leftAnalogDistance = 0.f;
		this.leftAnalogValues.set(0.f,0.f);
		this.rightAnalogAngle = 0.f;
		this.rightAnalogDistance = 0.f;
		this.rightAnalogValues.set(0.f,0.f);
	}
	
	public void update()
	{
		leftAnalogDistance = Controler.getAxisDistance(	controlerIdentifier,
														Controler.AxisXLeft,
														Controler.AxisYLeft,
														input);

		leftAnalogValues = Controler.getAxisValues(	controlerIdentifier,
													Controler.AxisXLeft,
													Controler.AxisYLeft,
													input);

		if( leftAnalogDistance > threshold )
		{
			leftAnalogAngle = Controler.getAxisAngle(	controlerIdentifier,
														Controler.AxisXLeft,
														Controler.AxisYLeft,
														input);

		}
		else
		{
			leftAnalogValues = new Vector2f();
			leftAnalogDistance = 0.f;
		}

		rightAnalogDistance = Controler.getAxisDistance(controlerIdentifier,
														Controler.AxisXRight,
														Controler.AxisYRight,
														input);

		rightAnalogValues = Controler.getAxisValues(controlerIdentifier,
													Controler.AxisXRight,
													Controler.AxisYRight,
													input);

		if( rightAnalogDistance > threshold )
		{

			rightAnalogAngle = Controler.getAxisAngle(	controlerIdentifier,
														Controler.AxisXRight,
														Controler.AxisYRight,
														input);
		}
		else
		{
			rightAnalogValues = new Vector2f();
			rightAnalogDistance = 0.f;
		}
	}
}
