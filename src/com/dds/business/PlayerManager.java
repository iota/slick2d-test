// ---------------------------------------
// PlayerManager.java
//
// Cr�e: 20 mars 2013 - 00:01:13
// Auteur: iota
// ---------------------------------------

package com.dds.business;

import java.util.Hashtable;
import java.util.Map;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class PlayerManager
{
	private Map<Integer,Player>	players;
	private UnicodeFont			shipFont;

	/**
	 * @throws SlickException
	 * 
	 */
	public PlayerManager() throws SlickException
	{
		this.players = new Hashtable<Integer,Player>();

		this.shipFont = new UnicodeFont("fonts/arial.ttf",
										40,
										true,
										false);
		this.shipFont.addAsciiGlyphs();
		this.shipFont.getEffects()
						.add(new ColorEffect(java.awt.Color.white));
		this.shipFont.loadGlyphs();
	}

	public Player addPlayer(char shipChar,Controler controler)
		throws SlickException
	{
		int playerID = this.players.size();
		Player player = createPlayer(shipChar,controler);

		this.players.put(playerID,player);

		return player;
	}

	public Player getPlayer(int playerID)
	{
		return this.players.get(playerID);
	}

	private Player createPlayer(char shipChar,Controler controler)
		throws SlickException
	{
		return new Player(	controler,
							ImageGenerator.createImage(	shipChar,
														this.shipFont));
	}
}
