import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.pbuffer.GraphicsFactory;

import com.dds.MainGame;

// ---------------------------------------
// MainClass.java
//
// Cr�e: 19 mars 2013 - 11:23:01
// Auteur: Administrateur
// ---------------------------------------

public class MainClass
{
	public static void main(String[] arguments)
	{
		GraphicsFactory.setUseFBO(true);

		AppGameContainer gameContainer;
		try
		{
			final MainGame game = new MainGame();
			gameContainer = new AppGameContainer(game,800,600,false);
			gameContainer.setMinimumLogicUpdateInterval(10);
			gameContainer.setMaximumLogicUpdateInterval(20);
			gameContainer.setVSync(true);
			gameContainer.start();
		}
		catch( final SlickException e )
		{
			e.printStackTrace();
		}

	}
}
